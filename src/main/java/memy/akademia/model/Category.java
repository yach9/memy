package memy.akademia.model;

import java.util.ArrayList;
import java.util.List;

public class Category {


    private long id;
    private String name;


    public Category() {
    }

    public Category(Long id, String name) {
        this.id = id;
        this.name = name;
    }
    private static List<Category> catList = new ArrayList<>();



    static {
        Category funny = new Category(0L, "Funny");
        Category android = new Category(1L, "Android");
        Category programming = new Category(2L, "Programming");
        catList.add(funny);
        catList.add(android);
        catList.add(programming);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Category> getCatList() {
        return catList;
    }

    public static void setCatList(List<Category> catList) {
        Category.catList = catList;
    }
}