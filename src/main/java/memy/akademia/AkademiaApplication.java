package memy.akademia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AkademiaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AkademiaApplication.class, args);
    }
}
